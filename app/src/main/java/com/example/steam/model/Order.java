
package com.example.steam.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;


public class Order {

    @SerializedName("MaPhieuDH")
    @Expose
    private Long id;

    @SerializedName("MaKH")
    @Expose
    private Long customerId;

    @SerializedName("Tong_SL_Dat")
    @Expose
    private Integer quantity;

    @SerializedName("ThanhTien")
    @Expose
    private Float total;

    @SerializedName("TinhTrang")
    @Expose
    private Integer status;

    @SerializedName("PhiShip")
    @Expose
    private Float shipFee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Float getShipFee() {
        return shipFee;
    }

    public void setShipFee(Float shipFee) {
        this.shipFee = shipFee;
    }

    Map<Integer, String> statusMap = new HashMap<>() {{
        put(-1, "Chưa Xác Nhận");
        put(0, "Xử lý");
        put(1, "Đã đóng gói");
        put(2, "Đang giao");
        put(3, "Giao thành công");
    }};
    public String getStatusString() {
        return statusMap.getOrDefault(status, "Chưa Xác Nhận");
    }
}