package com.example.steam.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Customer {

    @SerializedName("MaKH")
    @Expose
    private Long id;

    @SerializedName("TenKH")
    @Expose
    private String name;

    @SerializedName("DiaChi")
    @Expose
    private String address;

    @SerializedName("SDT")
    @Expose
    private String phoneNumber;

    @SerializedName("Email")
    @Expose
    private String email;

    @SerializedName("TenDN")
    @Expose
    private String username;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
