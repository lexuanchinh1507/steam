package com.example.steam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.steam.model.Customer;
import com.example.steam.model.LoginRequest;
import com.example.steam.model.Order;
import com.example.steam.utils.APIService;
import com.google.android.material.button.MaterialButton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView username = findViewById(R.id.username);
        TextView password = findViewById(R.id.password);

        MaterialButton loginbtn = findViewById(R.id.loginbtn);

        //admin and admin

        loginbtn.setOnClickListener(v -> {
            if (username.getText().toString().equals("") && password.getText().toString().equals("")) {
                Toast.makeText(MainActivity.this, "Username is password are not empty", Toast.LENGTH_SHORT).show();
            } else {
                handleLogin(username.getText().toString(), password.getText().toString());
            }
        });
    }

    private void handleLogin(String username, String password) {
        APIService.getInstance().login(new LoginRequest(username, password)).enqueue(new Callback<Customer>() {
            @Override
            public void onResponse(Call<Customer> call, Response<Customer> response) {
                if (response.body() != null) {
                    Toast.makeText(MainActivity.this, "Đăng nhập thành công", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(MainActivity.this, OrderActivity.class);
                    intent.putExtra("userId", response.body().getId());
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Username hoặc mật khẩu không chính xác", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Customer> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Username hoặc mật khẩu không chính xác", Toast.LENGTH_LONG).show();
            }
        });
    }
}