package com.example.steam.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.steam.R;
import com.example.steam.model.Order;

import java.util.List;

public class AdapterOrder extends RecyclerView.Adapter<AdapterOrder.ViewHolder> {

    List<Order> data;
    Context context;
    private IClickListener listener;

    public AdapterOrder(Context ctx) {
        this.context = ctx;
    }

    public void setData(List<Order> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setListener(IClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public AdapterOrder.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterOrder.ViewHolder viewHolder, int position) {
        Order order = data.get(position);
        if (order != null && order.getShipFee() != null) {
            viewHolder.shipFee.setText(order.getShipFee().toString());
            viewHolder.total.setText(order.getTotal().toString());
            viewHolder.id.setText(order.getId().toString());
            viewHolder.quantity.setText(order.getQuantity().toString());
            viewHolder.status.setText(order.getStatusString());
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface IClickListener {
        void onItemClick(Order order);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView id;
        public TextView quantity;
        public TextView total;
        public TextView status;
        public TextView shipFee;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);
            quantity = itemView.findViewById(R.id.quantity);
            total = itemView.findViewById(R.id.total);
            status = itemView.findViewById(R.id.status);
            shipFee = itemView.findViewById(R.id.shipFee);
        }

        @Override
        public void onClick(View view) {
            listener.onItemClick(data.get(getAdapterPosition()));
        }
    }
}
