package com.example.steam.utils;

import com.example.steam.model.Customer;
import com.example.steam.model.LoginRequest;
import com.example.steam.model.Order;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIInterface {
    @GET("/api/Order/getAllOrder")
    Call<List<Order>> getOrders();

    @POST("/api/Customer/POST_CreateCustomer")
    Call<Customer> login(@Body LoginRequest loginRequest);

}
