package com.example.steam;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.example.steam.adapter.AdapterOrder;
import com.example.steam.model.Order;
import com.example.steam.utils.APIService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderActivity extends AppCompatActivity {
    private List<Order> orders = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        Bundle extras = getIntent().getExtras();
        Long userId = extras.getLong("userId");
        RecyclerView rvOrder = (RecyclerView) findViewById(R.id.rvOrder);
        AdapterOrder adapterOrder = new AdapterOrder(this);
        adapterOrder.setData(orders);
        rvOrder.setAdapter(adapterOrder);
        rvOrder.setLayoutManager(new LinearLayoutManager(this));
        getOrders(userId, rvOrder, adapterOrder);
    }

    private void getOrders(Long userId, RecyclerView rvOrder, AdapterOrder adapterOrder) {
        APIService.getInstance().getOrders().enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<Order> data = response.body().stream().filter(order -> order.getCustomerId() == userId).collect(Collectors.toList());
                    orders.addAll(data);
                    adapterOrder.setData(orders);
                    rvOrder.setAdapter(adapterOrder);
                }
            }

            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {

            }
        });
    }
}